#!/bin/bash
echo "!!! STARTING IMPORT"
DBNAME="starterjs"

# Directory in docker container
# This should match the created dir path in ${ROOT}/mongodb/Dockerfile
DIR="/var/app/"

cd ./mongodb/seeds/

## Get Flags
DROP=false
while [[ $# -ge 1 ]]; do
  case "$1" in
      -d|--drop)
        DROP=true
        ;;
  esac
  shift
done

# Copy seed files
for i in `find . -name "*.json" -type f`; do
  docker cp $i starterjs_mongodb_1:$DIR
done

# Loop through all json files and create collection
for i in `find . -name "*.json" -type f`; do
    IN=$i
    arrFile=(${IN//// })
    file=${arrFile[1]}
    arrIN=(${IN//./ })
    col=(${arrIN[0]//// })

  if $DROP; then
    docker exec starterjs_mongodb_1 mongoimport --host mongodb --db $DBNAME --collection $col --type json --file $DIR$file --jsonArray --drop
  else
    docker exec starterjs_mongodb_1 mongoimport --host mongodb --db $DBNAME --collection $col --type json --file $DIR$file --jsonArray
  fi
done

echo "!!! IMPORT COMPLETE"