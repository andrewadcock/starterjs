import React from "react";
import "./App.css";
import { ApolloClient, ApolloProvider, InMemoryCache } from "@apollo/client";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import AboutPage from "./components/pages/about/AboutPage";
import UsersPage from "./components/pages/users/UsersPage";
import BearsPage from "./components/pages/bears/BearsPage";
import HomePage from "./components/pages/homepage/HomePage";
import BearEditPage from "./components/pages/bears/BearEditPage";
import BearAddPage from "./components/pages/bears/BearAddPage";
import UserAdd from "./components/components/users/UserAdd";

// Apollo client setup
const client = new ApolloClient({
  uri: "http://localhost:5000/graphql",
  cache: new InMemoryCache(),
});

function App() {
  return (
    <ApolloProvider client={client}>
      <Router>
        <Switch>
          <Route exact path="/">
            <HomePage />
          </Route>
          <Route exact path="/about">
            <AboutPage />
          </Route>
          <Route exact path="/users">
            <UsersPage />
          </Route>
          <Route exact path="/users/add">
            <UserAdd />
          </Route>
          <Route exact path="/bears">
            <BearsPage />
          </Route>
          <Route exact path="/bear/add">
            <BearAddPage />
          </Route>
          <Route exact path="/bear/:id">
            <BearEditPage />
          </Route>
        </Switch>
      </Router>
    </ApolloProvider>
  );
}

export default App;
