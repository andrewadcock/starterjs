/**
 * Interface: Bear
 */

export default interface IBear {
  id: string;
  species: string;
  color: string;
}
