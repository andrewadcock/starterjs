/**
 * Interface: User
 */
import IBear from "../bears/Bear";

export default interface IUser {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  favoriteBear: IBear;
}
