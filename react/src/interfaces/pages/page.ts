/**
 * Interface: Page
 */

import { History } from "history";

export default interface IPage {
  history?: History;
}
