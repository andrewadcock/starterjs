import { gql } from "@apollo/client";

const GET_BEARS = gql`
  {
    bears {
      id
      species
      color
    }
  }
`;

const ADD_USER = gql`
  mutation {
    addUser(
      firstName: "Belany"
      lastName: "Tonto"
      email: "test@example.com"
      favoriteBear: "613511359d677eb4196bfb38"
    )
  }
`;

const GET_USERS = gql`
  {
    users {
      id
      firstName
      lastName
      email
      favoriteBear {
        id
        species
        color
      }
      likedBears {
        id
        species
        color
      }
    }
  }
`;

export { GET_BEARS, GET_USERS, ADD_USER };
