import React from "react";
import IPage from "../../../interfaces/pages/page";
import { Link } from "react-router-dom";

const HomePage: React.FC<IPage> = (props) => {
  return (
    <div className="p-4">
      <h1>Welcome</h1>
      <ul>
        <li>
          <Link to="/users">Users</Link>
        </li>
        <li>
          <Link to="/bears">Bears</Link>
        </li>
        <li>
          <Link to="/bear/add">Add Bear</Link>
        </li>
        <li>
          <Link to="/users/add">Add User</Link>
        </li>
      </ul>
    </div>
  );
};

export default HomePage;
