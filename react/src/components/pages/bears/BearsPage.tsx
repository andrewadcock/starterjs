import React from "react";
import IPage from "../../../interfaces/pages/page";
import Bears from "../../components/bears/Bears";

const BearsPage: React.FC<IPage> = (props) => {
  return (
    <div className="p-4">
      <h1>Bears</h1>
      <Bears />
    </div>
  );
};

export default BearsPage;
