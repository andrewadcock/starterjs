import React from "react";
import BearEdit from "../../components/bears/BearEdit";
import { gql } from "@apollo/client";
import { graphql } from "@apollo/client/react/hoc";
import { Link } from "react-router-dom";

const GET_BEAR = gql`
  query bear($id: ID!) {
    bear(id: $id) {
      species
      color
    }
  }
`;

const BearEditPage: React.FC<any> = (props) => {
  return (
    <>
      <ul>
        <li>
          <Link to="/bears">All Bears</Link>
        </li>
      </ul>
      <BearEdit bear={props.data.bear} />
    </>
  );
};

export default graphql(GET_BEAR)(BearEditPage);
