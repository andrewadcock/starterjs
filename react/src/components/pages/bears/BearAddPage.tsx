import React from "react";
import { Link } from "react-router-dom";
import BearAdd from "../../components/bears/BearAdd";

const BearAddPage: React.FC<any> = (props) => {
  return (
    <>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/bears">All Bears</Link>
        </li>
      </ul>
      <BearAdd />
    </>
  );
};

export default BearAddPage;
