import React from "react";
import IPage from "../../../interfaces/pages/page";

const AboutPage: React.FC<IPage> = (props) => {
  return (
    <div className="p-4">
      <h1>About</h1>
    </div>
  );
};

export default AboutPage;
