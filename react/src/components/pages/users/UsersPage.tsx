import React from "react";
import IPage from "../../../interfaces/pages/page";
import Users from "../../components/users/Users";

const UsersPage: React.FC<IPage> = (props) => {
  return (
    <div className="p-4">
      <h1>Users</h1>
      <Users />
    </div>
  );
};

export default UsersPage;
