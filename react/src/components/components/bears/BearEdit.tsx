import React, { useEffect, useState } from "react";

const BearEdit: React.FC<any> = ({ bear }) => {
  const [currentBear, setCurrentBear] = useState<any>();

  useEffect(() => {
    setCurrentBear(bear);
  }, [bear]);

  const handleUpdate = () => {
    //
    return;
  };
  const updateBear = (e: any, column: any) => {
    if (currentBear && currentBear[column])
      setCurrentBear((currentBear[column] = e.target.value()));
  };

  return (
    <>
      <h1>Bear Edit Component</h1>
      {bear ? (
        <form>
          <label>
            Species
            <input
              type="text"
              value={bear.species}
              onClick={(e) => updateBear(e, "species")}
            />
          </label>
          <label>
            Color
            <input type="text" value={bear.color} />
          </label>
          <button type="submit" onClick={handleUpdate}>
            Update Date
          </button>
        </form>
      ) : (
        <p>Loading bear....</p>
      )}
    </>
  );
};

export default BearEdit;
