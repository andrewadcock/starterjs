import React, { useEffect, useState } from "react";

const BearAdd: React.FC<any> = ({ bear }) => {
  const [currentBear, setCurrentBear] = useState<any>({
    species: "",
    color: "",
  });

  useEffect(() => {
    setCurrentBear(bear);
  }, [bear]);

  const handleAdd = () => {
    //
    return;
  };
  const updateBear = (e: any, column: any) => {
    const updatedBear = { ...currentBear };
    updatedBear[column] = e;
    setCurrentBear(updatedBear);
  };

  return (
    <>
      <h1>Bear Edit Component</h1>

      <form>
        <label>
          Species
          <input
            type="text"
            value={currentBear && currentBear.species}
            onChange={(e: any) => updateBear(e.currentTarget.value, "species")}
          />
        </label>
        <label>
          Color
          <input
            type="text"
            value={currentBear && currentBear.color}
            onChange={(e: any) => updateBear(e.currentTarget.value, "color")}
          />
        </label>
        <button type="submit" onClick={handleAdd}>
          Create Bear
        </button>
      </form>

      {currentBear ? (
        <p>
          Species: {currentBear.species}, Color: {currentBear.color}
        </p>
      ) : null}
    </>
  );
};

export default BearAdd;
