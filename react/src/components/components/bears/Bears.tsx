import React, { useEffect, useState } from "react";
import IBear from "../../../interfaces/components/bears/Bear";
import { graphql } from "@apollo/client/react/hoc";
import { GET_BEARS } from "../../../queries/queries";

const Bears: React.FC<any> = (props) => {
  const [isLoading, setIsLoading] = useState<boolean>();
  console.log("props", props);
  // Generate list items for bears
  const displayBears = () => {
    if (!props.data.loading) {
      if (!props.data?.bears || !props.data?.bears.length) {
        return <p>No bears.</p>;
      }
      return props?.data?.bears.map((bear: IBear, index: number) => {
        return (
          <li key={index}>
            <a
              href={`/bear/${bear.id}`}
            >{`Species: ${bear.species}, Color: ${bear.color}`}</a>
          </li>
        );
      });
    } else {
      return <p>Loading Bears</p>;
    }
  };

  useEffect(() => {
    setIsLoading(props.data.isLoading);
  }, [props]);
  return (
    <>
      <h2>Bears</h2>
      {isLoading ? <p>Loading</p> : null}
      <ul>{displayBears()}</ul>
    </>
  );
};

export default graphql(GET_BEARS)(Bears);
