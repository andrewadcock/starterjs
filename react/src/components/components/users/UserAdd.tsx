import React, { useState } from "react";
import { GET_BEARS, ADD_USER } from "../../../queries/queries";
import { graphql } from "@apollo/client/react/hoc";
import IBear from "../../../interfaces/components/bears/Bear";
import { flowRight as compose } from "lodash";

const UserAdd: React.FC<any> = ({ bear, getBears, addUser }) => {
  const [firstName, setFirstName] = useState<string>();
  const [lastName, setLastName] = useState<string>();
  const [email, setEmail] = useState<string>();
  const [favoriteBearId, setFavoriteBearId] = useState<string>();

  const displayBears = () => {
    if (getBears.loading) {
      return <option disabled={true}>Loading Bears</option>;
    } else {
      return getBears.bears.map((bear: IBear) => {
        return (
          <option value={bear.id} key={bear.id}>
            {bear.species}
          </option>
        );
      });
    }
  };

  const handleSubmit = (e: React.SyntheticEvent) => {
    e.preventDefault();
    addUser();
  };

  return (
    <>
      <h1>Add User</h1>

      <form onSubmit={(e) => handleSubmit(e)}>
        <label>
          First Name
          <input
            type="text"
            onChange={(e) => setFirstName(e.currentTarget.value)}
          />
        </label>
        <label>
          Last Name
          <input
            type="text"
            onChange={(e) => setLastName(e.currentTarget.value)}
          />
        </label>
        <label>
          Email
          <input
            type="text"
            onChange={(e) => setEmail(e.currentTarget.value)}
          />
        </label>
        <label>
          Favorite Bear
          <select onChange={(e) => setFavoriteBearId(e.currentTarget.value)}>
            <option>Select Bear</option>
            {displayBears()}
          </select>
        </label>
        <button type="submit">Create User</button>
      </form>
    </>
  );
};

export default compose(
  graphql(GET_BEARS, { name: "getBears" }),
  graphql(ADD_USER, { name: "addUser" })
)(UserAdd);
