import React, { useEffect, useState } from "react";
import { graphql } from "@apollo/client/react/hoc";
import MaterialTable from "material-table";
import { GET_USERS } from "../../../queries/queries";

const USER_COLUMNS = [
  { title: "First Name", field: "firstName" },
  { title: "Last Name", field: "lastName" },
  { title: "Email", field: "email" },
  { title: "Favorite Bear", field: "favoriteBear" },
  { title: "Liked Bears", field: "likedBears" },
];

const Users: React.FC<any> = (props) => {
  const [isLoading, setIsLoading] = useState<boolean>();
  const [data, setData] = useState<any>();

  useEffect(() => {
    // Start loader
    setIsLoading(props.data.isLoading);

    if (!props.data.loading && props?.data?.users) {
      // Make shallow copy
      const tempData = [...props.data.users];

      // Sort by firstName ascending
      const sortedData = tempData.sort((a: any, b: any) => {
        if (a.firstName < b.firstName) return -1;

        if (a.firstName > b.firstName) return 1;

        return 0;
      });

      setData(sortedData);
    }
  }, [props]);
  return (
    <>
      {isLoading ? <p>Loading Users...</p> : null}
      <div>
        {data ? (
          <MaterialTable
            columns={USER_COLUMNS}
            data={data.map((d: any) => {
              const likedBears = d.likedBears.map((bear: any) => {
                return (
                  <ul>
                    <li>
                      <a href={`/bear/${bear.id}`}>{bear.species}</a>
                    </li>
                  </ul>
                );
              });

              return {
                firstName: d.firstName,
                lastName: d.lastName,
                email: d.email,
                favoriteBear: (
                  <a href={`/bear/${d.favoriteBear.id}`}>
                    {d.favoriteBear.species}/{d.favoriteBear.color}
                  </a>
                ),
                likedBears: likedBears,
              };
            })}
            title="Users"
          />
        ) : null}
      </div>
    </>
  );
};

export default graphql(GET_USERS)(Users);
