import IRoute from "../src/interfaces/config/routes";
import HomePage from "../src/components/pages/homepage/HomePage";
import AboutPage from "../src/components/pages/about/AboutPage";
import BearsPage from "../src/components/pages/bears/BearsPage";
import BearEditPage from "../src/components/pages/bears/BearEditPage";

const routes: IRoute[] = [
  {
    path: "/",
    name: "homepage",
    component: HomePage,
    exact: true,
  },
  {
    path: "/about",
    name: "about",
    component: AboutPage,
    exact: true,
  },
  {
    path: "/bears",
    name: "bears",
    component: BearsPage,
    exact: true,
  },
  {
    path: "/bear/:id",
    name: "bearEdit",
    component: BearEditPage,
    exact: true,
  },
];

export default routes;
