global.TextEncoder = require("util").TextEncoder;
global.TextDecoder = require("util").TextDecoder;
const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const mongoose = require("mongoose");
const schema = require("./schemas/schema");
const cors = require("cors");

const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");

const { graphqlHTTP } = require("express-graphql");

const app = express();

// Use cors
app.use(cors());

// Set up Mongoose
mongoose
  .connect(`mongodb://mongodb:27017/${process.env.DBNAME || "starterjs"}`)
  .catch((err) => {
    console.log(`Mongoose error: ${err}`);
  });
mongoose.connection.once("open", () => {
  console.log("Mongoose connection successful");
});

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);

// GraphQL
app.use(
  "/graphql",
  graphqlHTTP({
    schema,
    graphiql: true,
  })
);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
