const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const bearSchema = new Schema({
  color: String,
  species: String,
});

module.exports = mongoose.model('Bear', bearSchema);