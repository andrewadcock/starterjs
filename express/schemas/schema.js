const {
  GraphQLBoolean,
  GraphQLObjectType,
  GraphQLString,
  GraphQLSchema,
  GraphQLID,
  GraphQLList,
  GraphQLNonNull,
} = require("graphql");
const _ = require("lodash");
const Bear = require("../models/bear");
const User = require("../models/user");

const UserType = new GraphQLObjectType({
  name: "User",
  fields: () => ({
    id: { type: GraphQLID },
    firstName: { type: GraphQLString },
    lastName: { type: GraphQLString },
    email: { type: GraphQLString },
    isAdmin: { type: GraphQLBoolean },
    favoriteBear: {
      type: BearType,
      resolve(parent, args) {
        return Bear.findById(parent.favoriteBear);
      },
    },
    likedBears: {
      type: new GraphQLList(BearType),
      resolve(parent, args) {
        // Loop through liked bears and find matches
        return parent.likedBears.map((id) => {
          return Bear.findById(id);
        });
      },
    },
  }),
});

const BearType = new GraphQLObjectType({
  name: "bear",
  fields: () => ({
    id: { type: GraphQLID },
    color: { type: GraphQLString },
    species: { type: GraphQLString },
  }),
});

const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {
    bear: {
      type: BearType,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        return Bear.findById(args.id);
      },
    },
    bears: {
      type: new GraphQLList(BearType),
      resolve(parent, args) {
        return Bear.find({});
      },
    },
    user: {
      type: UserType,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        return User.findById(args.id);
      },
    },
    users: {
      type: new GraphQLList(UserType),
      resolve(parent, args) {
        return User.find({});
      },
    },
  },
});

const Mutation = new GraphQLObjectType({
  name: "Mutation",
  fields: {
    addBear: {
      type: BearType,
      args: {
        color: { type: new GraphQLNonNull(GraphQLString) },
        species: { type: new GraphQLNonNull(GraphQLString) },
      },
      resolve(parent, args) {
        let bear = new Bear({
          color: args.color,
          species: args.species,
        });

        return bear.save();
      },
    },
    removeBear: {
      type: BearType,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        return Bear.findByIdAndRemove(args.id);
      },
    },
    addUser: {
      type: UserType,
      args: {
        firstName: { type: new GraphQLNonNull(GraphQLString) },
        lastName: { type: GraphQLString },
        email: { type: new GraphQLNonNull(GraphQLString) },
        isAdmin: { type: GraphQLBoolean },
        favoriteBear: { type: new GraphQLNonNull(GraphQLID) },
        // likedBears: {type: new GraphQLList(Gr) }
      },
      resolve(parent, args) {
        let user = new User({
          firstName: args.firstName,
          lastName: args.lastName,
          email: args.email,
          isAdmin: args.isAdmin,
          favoriteBear: args.favoriteBear,
          // likedBears: args.likedBears
        });

        return user.save();
      },
    },
    removeUser: {
      type: UserType,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        return User.findByIdAndRemove(args.id);
      },
    },
  },
});

module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation,
});
